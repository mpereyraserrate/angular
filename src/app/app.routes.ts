import { Routes, RouterModule } from "@angular/router";


export const ROUTES: Routes = [
    /* { path: 'home', component: }, */
    { path: '', pathMatch: 'full', redirectTo:'home'},
    { path: '**', pathMatch: 'full', redirectTo:'home'}
];
