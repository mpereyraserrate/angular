import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import * as jQuery from 'jquery';
import Swal from 'sweetalert2';
import { ThisReceiver } from '@angular/compiler';
import { CookieService } from 'ngx-cookie-service';


declare var $: any;
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  
  form!: FormGroup;
  products_: any [] = [];
  id: any;
  product: Array <any>;
  description: any;
  
  current_page: number = 1;
  capture: number;
  states: any [] = [];
  filterPost: string = "";
  filterPost2: string = "";
  formProduct: Array<any>  = [];

  estado: string = "";
  user_id: number = 0;
  constructor(private router: Router, private fb: FormBuilder, 
              private productservice: ProductService,
              private coockie_service: CookieService) { 
    this.products_ = [];
    this.user_id = +sessionStorage.getItem('user_id')!;
    this.product = [];
    this.states = [];
    this.capture = 1;
    console.log(this.products_);
    console.log(this.capture);
    this.get_products();
    this.get_all_states();
  }
  




  productsForm(){
    this.form = this.fb.group({
      date: ['', Validators.required],
      analist: ['', Validators.required],
      category: ['', Validators.required],
      atc_code: ['', Validators.required],
      structure: ['', Validators.required],
      terapeutic_efect: ['', Validators.required],
      action_mechanism: ['', Validators.required]
      
    })
  }
  addProduct(){
     let newProduct = {
        id: this.id,
        product: this.product,
        description: this.description,
     } 
     this.products_.push(newProduct)
     this.router.navigate(['/dashboard/products'])
        /* .then(()=>{
          window.location.reload();
        }) */
     console.log(this.products_);
     
  }
  ngOnInit(): void {
    /* const signupButton = document.querySelector('#signup');
    const modalBg = document.querySelector('.modal-background');
    const modal = document.querySelector('.modal');
    
    signupButton?.addEventListener('click', () =>{
      modal?.classList.add('is-active');
    });
    modalBg?.addEventListener('click', () =>{
      modal?.classList.remove('is-active');
    }); */
   
    /* jQuery(document).on('click', '#signup',function(){
      //alert('Hola');
      const modalBg = document.querySelector('#close');
      const modal = document.querySelector('.modal');
     
        modal?.classList.add('is-active');

        modalBg?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
        
    }) */
  }
  view(id: any){
    sessionStorage.setItem("product_id_analist", id);
    this.router.navigate(['/dashboard/formAnalists']);
  }
  objectKeys (objeto: any) {
    const keys = Object.keys(objeto);
    console.log(keys); // echa un vistazo por consola para que veas lo que hace "Object.keys"
    return keys;
 }
  approve(){
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Producto Aprobado para Completar',
      showConfirmButton: false,
      timer: 1500
    });
  }
  get_products(){
    let products = {
      estados : '2',
      user_id : 0,
    };
    this.productservice.get_all_products(products)
        .subscribe(
          (res: any) => {
            this.products_ = res.detalle;
            console.log(this.products_);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_all_states(){
    let states = null;
    this.productservice.all_states(states)
        .subscribe(
          (res: any) => {
            this.states = res.detalle;
            console.log(this.states);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  submit(){
    
  }
}
