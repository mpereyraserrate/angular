import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, NgForm  } from '@angular/forms';
import { Router } from '@angular/router';
import { NgSelectConfig } from '@ng-select/ng-select';
import { CookieService } from 'ngx-cookie-service';
import { ProductService } from 'src/app/services/product.service';
import * as jQuery from 'jquery';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-view-product-information-doctors',
  templateUrl: './view-product-information-doctors.component.html',
  styleUrls: ['./view-product-information-doctors.component.css']
})
export class ViewProductInformationDoctorsComponent implements OnInit {
  product_id: any = 0;
  products: Array <any>;
  analist_information: Array <any>;
  list_products: any;
  form_doctors!: FormGroup;
  constructor(private router: Router, private fb: FormBuilder, 
              private product_service: ProductService,
              private http: HttpClient,
              private cdr: ChangeDetectorRef,
              private config: NgSelectConfig,
              private cookieservice: CookieService) { 
    this.products = [];
    this.analist_information = [];
    this.product_id = +sessionStorage.getItem('product_id_view_doctor')!;
    this.get_product_id();
}

  ngOnInit(): void {
    jQuery(document).on('click', '#analist_form',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.remove('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.add('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.remove('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.add('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information2',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.remove('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.add('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information3',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.remove('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.add('is-active');
    });
  }
  get_product_id(){
    let product_id = {
      id: this.product_id,
    }
    this.product_service.get_product_id(product_id)
        .subscribe(
          (res: any) => {
            this.products = res.detalle;
            console.log(this.products);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  approve(product_id: any){
    let product_information = {
      id: product_id,
      user_id: +sessionStorage.getItem('user_id')!,
      estado_id: 4,
    }
    this.product_service.update_states(product_information)
        .subscribe(
          (res: any) => {
            console.log(res);
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: res.mensaje,
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/productListDoctors'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500); */
          },
          (error: any) => {
            console.log(error.error);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Error al Actualizar el Estado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/productListDoctors'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500) */
          }
        );
  }
  disapprove(product_id: any){
    let product_information = {
      id: product_id,
      user_id: +sessionStorage.getItem('user_id')!,
      estado_id: 5,
    }
    this.product_service.update_states(product_information)
        .subscribe(
          (res: any) => {
            /* console.log(res); */
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: res.mensaje + 'Producto Rechazado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/productListDoctors'])
                        /* setTimeout(() => {
                          window.location.reload();
                        }, 2500) */
          },
          (error: any) => {
            console.log(error.error);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Error al Actualizar el Estado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/productListDoctors'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500) */
          }
        );
  }
}
