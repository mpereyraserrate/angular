import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProductInformationDoctorsComponent } from './view-product-information-doctors.component';

describe('ViewProductInformationDoctorsComponent', () => {
  let component: ViewProductInformationDoctorsComponent;
  let fixture: ComponentFixture<ViewProductInformationDoctorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewProductInformationDoctorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProductInformationDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
