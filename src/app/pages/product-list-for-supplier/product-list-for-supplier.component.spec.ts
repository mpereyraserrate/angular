import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListForSupplierComponent } from './product-list-for-supplier.component';

describe('ProductListForSupplierComponent', () => {
  let component: ProductListForSupplierComponent;
  let fixture: ComponentFixture<ProductListForSupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductListForSupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListForSupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
