import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';
import * as jQuery from 'jquery';
import { CookieService } from 'ngx-cookie-service';
declare var $: any;
@Component({
  selector: 'app-product-list-for-supplier',
  templateUrl: './product-list-for-supplier.component.html',
  styleUrls: ['./product-list-for-supplier.component.css']
})
export class ProductListForSupplierComponent implements OnInit {
  products_: Array <any>;
  products: Array <any>;
  states: Array <any> = [];
  current_page: number = 1;
  filterPost: string = "";
  filterPost2: string = "";
  constructor(private product_service: ProductService, private router: Router,
              private cookie_service: CookieService) {
    this.products_ = [];
    this.products = [];
    
    /* this.states = this.product_service.states; */
    this.get_states();
    this.get_all_products();
    
   }

  ngOnInit(): void {
    /* var row; //Captur all the information in one row
    var id;
    var product;
    var description;
    $(document).on("click",".btnEdit", function(this: any){
      const modalBg = document.querySelector('#close');
      const modal = document.querySelector('.modal');
     
        modal?.classList.add('is-active');

        modalBg?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
      row = $(this).closest("tr");
      id = parseInt(row.find('td:eq(0)').text());
      product = row.find('td:eq(1)').text();
      description = row.find('td:eq(2)').text();

      $("#product").val(product);
      $("#description").val(description);
      
    }); */
  }
  approve(){
    
  }
  view(id: any){
    sessionStorage.setItem('product_update_id', id);
    this.router.navigate(['/dashboard/updateProduct']);
  }
  get_states(){
    let state = null;
    this.product_service.all_states(state)
        .subscribe(
          (res: any) => {
            this.states = res.detalle;
            console.log(this.states);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  get_all_products(){
    var id = +sessionStorage.getItem('user_id')!;
    let products = {
      estados: '1,2,3,4,5',
      user_id: id,
    }
    this.product_service.get_all_products4(products)
        .subscribe(
          (res: any) => {
            this.products_ = res.detalle;
            console.log(res);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  ChackValue(value:any){
    try {
    return value();
    } catch (error) {
    console.log(error);
    }
  }
}
