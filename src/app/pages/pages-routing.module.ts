import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { RouterModule, Routes} from '@angular/router';
import { StockComponent } from './stock/stock.component';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { GuardGuard } from '../guards/guard.guard';
import { ProductListForSupplierComponent } from './product-list-for-supplier/product-list-for-supplier.component';
import { ProductListDoctorsComponent } from './product-list-doctors/product-list-doctors.component';
import { AdditionalInformationAnalistsComponent } from './additional-information-analists/additional-information-analists.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { ViewProductInformationDoctorsComponent } from './view-product-information-doctors/view-product-information-doctors.component';
import { ListProductAnalistBossComponent } from './list-product-analist-boss/list-product-analist-boss.component';
import { ViewProductAnalistBossComponent } from './view-product-analist-boss/view-product-analist-boss.component';
import { SupplierAssignmentsComponent } from './supplier-assignments/supplier-assignments.component';
import { AnalistsAssignmentsComponent } from './analists-assignments/analists-assignments.component';

const routes: Routes = [
  {path: 'dashboard', component: PagesComponent,
    canActivate: [GuardGuard],
    children: [
      {path: '', component: DashboardComponent, data: {title: 'Dashboard'}},
      {path: 'users', component: UsersComponent, data: {title: 'Usuarios'}},
      {path: 'listAnalists', component: ProductsComponent, data: {title: 'Lista de Productos(Analistas)'}},
      {path: 'addproducts', component: AddproductsComponent, data: {title: 'Registrar Productos'}},
      {path: 'productList', component: ProductListForSupplierComponent, data: {title: 'Lista de Productos'}},
      {path: 'productListDoctors', component: ProductListDoctorsComponent, data: {title: 'Lista de Productos Doctores'}},
      {path: 'formAnalists', component: AdditionalInformationAnalistsComponent, data: {title: 'Datos Adicionales Analistas'}},
      {path: 'updateProduct', component: UpdateProductComponent, data: {title: 'Actualizar Producto'}},
      {path: 'viewDoctors', component: ViewProductInformationDoctorsComponent, data: {title: 'Vista Producto Doctor'}},
      {path: 'listBossAnalist', component: ListProductAnalistBossComponent, data: {title: 'Lista Jefe Analista'}},
      {path: 'viewBossAnalist', component: ViewProductAnalistBossComponent, data: {title: 'Vista Jefe Analista'}},
      {path: 'supplierAssignments', component: SupplierAssignmentsComponent, data: {title: 'Asignaciones Proveedores'}},
      {path: 'analistAssignments', component: AnalistsAssignmentsComponent, data: {title: 'Asignaciones Analistas'}},
    ]
  },
  { path: '**', pathMatch: 'full', redirectTo:'home'}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PagesRoutingModule { }
