import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ProductService } from 'src/app/services/product.service';
import * as jQuery from 'jquery';
import { AssignmentService } from '../../services/assignment.service';
import { GroupedObservable } from 'rxjs';
import Swal from 'sweetalert2';
declare var $: any;
@Component({
  selector: 'app-supplier-assignments',
  templateUrl: './supplier-assignments.component.html',
  styleUrls: ['./supplier-assignments.component.css']
})
export class SupplierAssignmentsComponent implements OnInit {
  current_page: number = 1;
  filterPost: string = "";
  filterPost2: string = "";
  filterPost3: string = "";
  filterPost4: string = "";
  filterPost5: string = "";
  list_products: Array <any>;
  states: any [] = [];
  suppliers: Array <any>;
  lines: Array <any>;
  sub_lines: Array <any>;
  groups: Array <any>;
  supplier_code: any;
  line_code: any;
  sub_line_code: any;
  group_code: any;

  suppliers_assignments: Array <any>;
  constructor(private router: Router, private fb: FormBuilder, 
              private productService: ProductService,
              private http: HttpClient,
              private cdr: ChangeDetectorRef,
              private config: NgSelectConfig,
              private product_service: ProductService,
              private assignment_service: AssignmentService) { 

    this.list_products = [];
    this.states = [];
    this.suppliers = [];
    this.lines = [];
    this.sub_lines = [];
    this.groups = [];
    this.suppliers_assignments = [];
    this.config.notFoundText = 'Dato no Encontrado';
    this.get_products();
    this.get_states();
    this.get_supplier_users();
    this.get_line();
    this.get_sub_line();
    this.get_groups();
    this.get_assignments();
  }
  
  ngOnInit(): void {
    $(document).on("click",".btnEdit", function(this: any){
      const modalBg = document.querySelector('#close');
      const modal = document.querySelector('.modal');
      const modalBg2 = document.querySelector('.close2');

        modal?.classList.add('is-active');

        modalBg?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
        modalBg2?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
      });
  }
  get_states(){
    let states = null;
    this.product_service.all_states(states)
        .subscribe(
          (res: any) => {
            this.states = res.detalle;
            console.log(this.states);
          },
          (error: any) => {
            console.log(error.error);
            
          }
        );
  }
  get_products(){
    var id =  +sessionStorage.getItem('user_id')!;
    let products = {
      estados: '1,2,3,4,5',
      user_id: id,
    }
    this.product_service.get_all_products2(products)
        .subscribe(
          (res: any) => {
            this.list_products = res.detalle;
            console.log(this.list_products);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  get_supplier_users(){
    let users = {
      codigoperfil: 2,
      estado: 1,
    }
    this.assignment_service.get_perfil_users(users)
        .subscribe(
          (res: any) => {
            this.suppliers = res.detalle;
            console.log(this.suppliers);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_line(){
    let line = {
      codigoLinea: 0
    }
    this.assignment_service.get_lines(line)
        .subscribe(
          (res: any) => {
            this.lines = res.detalle;
            console.log(this.lines);
            
          },
          (error: any) => {
            console.log(error);
            
          }
        );
  }
  get_sub_line(){
    let sub_line = {
      codigosubLinea: 0,
    }
    this.assignment_service.get_sub_lines(sub_line)
        .subscribe(
          (res: any) => {
            this.sub_lines = res.detalle
            console.log(this.sub_lines);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_groups(){
    let group = {
      codigoGrupo: 0,
    }
    this.assignment_service.get_groups(group)
        .subscribe(
          (res: any) => {
            this.groups = res.detalle;
            console.log(this.groups);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  save_assignments(){
    let information = {
      codigoproveedor: this.supplier_code,
      codigolinea: this.line_code,
      codigosublinea: this.sub_line_code,
      codigogrupo: this.group_code,
      estado: 1,
      user_id: +sessionStorage.getItem('user_id')!
    }
    this.assignment_service.save_supplier_assignments(information)
        .subscribe(
          (res: any) => {
            console.log(res);
            if(res.estado == 1){
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 1500
              }
              );
            }else{
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 1500
              }
              );
            }
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_assignments(){
    let information = {
      codigoproveedor: 0,
    }
    this.assignment_service.get_suppliers_assignments(information)
        .subscribe(
          (res: any) => {
            this.suppliers_assignments =res.detalle;
            console.log(this.suppliers_assignments);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
}
