import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierAssignmentsComponent } from './supplier-assignments.component';

describe('SupplierAssignmentsComponent', () => {
  let component: SupplierAssignmentsComponent;
  let fixture: ComponentFixture<SupplierAssignmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplierAssignmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
