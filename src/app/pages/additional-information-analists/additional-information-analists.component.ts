import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import * as jQuery from 'jquery';
import { timer } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ProductService } from '../../services/product.service';
declare var $: any;
@Component({
  selector: 'app-additional-information-analists',
  templateUrl: './additional-information-analists.component.html',
  styleUrls: ['./additional-information-analists.component.css']
})
export class AdditionalInformationAnalistsComponent implements OnInit {
  @Input() id: any;
  product_id: any = 0;
  form !: FormGroup;
  submitted: boolean = false;
  user_id: number = 0;
  product: Array <any>;
  constructor(private router: Router, private fb: FormBuilder, 
              private cdr: ChangeDetectorRef,
              private cookie_service: CookieService,
              private product_service: ProductService) { 
    this.product_id = +sessionStorage.getItem('product_id_analist')!;
    this.user_id = +sessionStorage.getItem('user_id')!;
    this.product = [];
    this.analistForm();
    this.get_product_id();
  }

  ngOnInit(): void {
    jQuery(document).on('click', '#analist_form',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.remove('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.add('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.remove('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.add('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information2',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.remove('is-hidden');
      product_information3?.classList.add('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.add('is-active');
      tab4?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information3',function(){
      const analist_form = document.querySelector('#form');
      const product_information = document.querySelector('.form_product_information');
      const product_information2 = document.querySelector('.form_product_information2');
      const product_information3 = document.querySelector('.form_product_information3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');
      const tab4 = document.querySelector('.tab4');

      analist_form?.classList.add('is-hidden');
      product_information?.classList.add('is-hidden');
      product_information2?.classList.add('is-hidden');
      product_information3?.classList.remove('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.remove('is-active');
      tab4?.classList.add('is-active');
    });
   
  }
  analistForm(){
    this.form = this.fb.group({
      date: ['', [Validators.required, Validators.minLength(3)]],
      analist_name: ['', [Validators.required, Validators.minLength(3)]],
      category: ['', [Validators.required, Validators.minLength(3)]],
      atc_code: [''],
      first_level: ['', [Validators.required, Validators.minLength(3)]],
      second_level: ['', [Validators.required, Validators.minLength(3)]],
      third_level: ['', [Validators.required, Validators.minLength(3)]],
      fourth_level: ['', [Validators.required, Validators.minLength(3)]],
      fifth_level: ['', [Validators.required, Validators.minLength(3)]],
      /* line: ['', [Validators.required, Validators.minLength(3)]],
      sub_line: ['', [Validators.required, Validators.minLength(3)]],
      groups: ['', [Validators.required, Validators.minLength(3)]], */
      second_category: ['', [Validators.required, Validators.minLength(3)]],
      family: ['', [Validators.required, Validators.minLength(3)]],
      sub_family: ['', [Validators.required, Validators.minLength(3)]],
      supplier_code: ['', [Validators.required, Validators.max(99999999999999999999), Validators.pattern(/^[0-9]\d*$/)]],
      supllier_name: ['', [Validators.required, Validators.minLength(3)]],
      credit_time: [''],
      product_id_fk: this.product_id,
      user_id_analista: this.user_id
      //Principio activo, concentracion, molecula
      //aplicaiones, observaciones
      //linea, sublinea, grupo
      //categoria, familia, subfamilia
      //codigo proveedor, nombre proveedor, tiempo de credito
    })
  }
  /* ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  } */
  get date_Field(): AbstractControl | null | undefined {
    return this.form?.get('date');
  }
  get date_Valid(){
    return this.date_Field?.valid;
  }
  get date_Invalid(){
    return  this.date_Field?.invalid && (this.date_Field?.dirty || this.date_Field?.touched);
  }
  get name_analist_Field(): AbstractControl | null | undefined {
    return this.form?.get('analist_name');
  }
  get name_analist_Valid(){
    return this.name_analist_Field?.valid;
  }
  get name_analist_Invalid(){
    return  this.name_analist_Field?.invalid && (this.name_analist_Field?.dirty || this.name_analist_Field?.touched);
  }
  get category_Field(): AbstractControl | null | undefined {
    return this.form?.get('category');
  }
  get category_Valid(){
    return this.category_Field?.valid;
  }
  get category_Invalid(){
    return  this.category_Field?.invalid && (this.category_Field?.dirty || this.category_Field?.touched);
  }
  get atc_code_Field(): AbstractControl | null | undefined{
    return this.form?.get('atc_code');
  }
  get atc_code_Valid(){
    return this.atc_code_Field?.valid;
  }
  get atc_code_Invalid(){
    /* return  this.atc_code_Field?.invalid && (this.atc_code_Field?.dirty || this.atc_code_Field?.touched); */
    return  this.atc_code_Field?.dirty;
  }
  get first_level_Field(): AbstractControl | null | undefined {
    return this.form?.get('first_level');
  }
  get first_level_Valid(){
    return this.first_level_Field?.valid;
  }
  get first_level_Invalid(){
    return  this.first_level_Field?.invalid && (this.first_level_Field?.dirty || this.first_level_Field?.touched);
  }
  get second_level_Field(): AbstractControl | null | undefined {
    return this.form?.get('second_level');
  }
  get second_level_Valid(){
    return this.second_level_Field?.valid;
  }
  get second_level_Invalid(){
    return  this.second_level_Field?.invalid && (this.second_level_Field?.dirty || this.second_level_Field?.touched);
  }
  get third_level_Field(): AbstractControl | null | undefined {
    return this.form?.get('third_level');
  }
  get third_level_Valid(){
    return this.third_level_Field?.valid;
  }
  get third_level_Invalid(){
    return  this.third_level_Field?.invalid && (this.third_level_Field?.dirty || this.third_level_Field?.touched);
  }
  get fourth_level_Field(): AbstractControl | null | undefined {
    return this.form?.get('fourth_level');
  }
  get fourth_level_Valid(){
    return this.fourth_level_Field?.valid;
  }
  get fourth_level_Invalid(){
    return  this.fourth_level_Field?.invalid && (this.fourth_level_Field?.dirty || this.fourth_level_Field?.touched);
  }
  get fifth_level_Field(): AbstractControl | null | undefined {
    return this.form?.get('fifth_level');
  }
  get fifth_level_Valid(){
    return this.fifth_level_Field?.valid;
  }
  get fifth_level_Invalid(){
    return  this.fifth_level_Field?.invalid && (this.fifth_level_Field?.dirty || this.fifth_level_Field?.touched);
  }
  get line_Field(): AbstractControl | null | undefined {
    return this.form?.get('line');
  }
  get line_Valid(){
    return this.line_Field?.valid;
  }
  get line_Invalid(){
    return this.line_Field?.invalid && (this.line_Field?.dirty || this.line_Field?.touched);
  }
  get sub_line_Field(): AbstractControl | null | undefined {
    return this.form?.get('sub_line');
  }
  get sub_line_Valid(){
    return this.sub_line_Field?.valid;
  }
  get sub_line_Invalid(){
    return this.sub_line_Field?.invalid && (this.sub_line_Field?.dirty || this.sub_line_Field?.touched);
  }
  get group_Field(): AbstractControl | null | undefined {
    return this.form?.get('groups');
  }
  get group_Valid(){
    return this.group_Field?.valid;
  }
  get group_Invalid(){
    return this.group_Field?.invalid && (this.group_Field?.dirty || this.group_Field?.touched);
  }
  get second_category_Field(): AbstractControl | null | undefined {
    return this.form?.get('second_category');
  }
  get second_category_Valid(){
    return this.second_category_Field?.valid;
  }
  get second_category_Invalid(){
    return this.second_category_Field?.invalid && (this.second_category_Field?.dirty || this.second_category_Field?.touched);
  }
  get family_Field(): AbstractControl | null | undefined{
    return this.form?.get('family');
  }
  get family_Valid(){
    return this.family_Field?.valid;
  }
  get family_Invalid(){
    return this.family_Field?.invalid && (this.family_Field?.dirty || this.family_Field?.touched);
  }
  get sub_family_Field(): AbstractControl | null | undefined{
    return this.form?.get('sub_family');
  }
  get sub_family_Valid(){
    return this.sub_family_Field?.valid;
  }
  get sub_family_Invalid(){
    return this.sub_family_Field?.invalid && (this.sub_family_Field?.dirty || this.sub_family_Field?.touched);
  }
  get supplier_code_Field(): AbstractControl | null | undefined{
    return this.form?.get('supplier_code');
  }
  get supplier_code_Valid(){
    return this.supplier_code_Field?.valid;
  }
  get supplier_code_Invalid(){
    return this.supplier_code_Field?.invalid && (this.supplier_code_Field?.dirty || this.supplier_code_Field?.touched);
  }
  get supplier_name_Field(): AbstractControl | null | undefined{
    return this.form?.get('supllier_name');
  }
  get supplier_name_Valid(){
    return this.supplier_name_Field?.valid;
  }
  get supplier_name_Invalid(){
    return this.supplier_name_Field?.invalid && (this.supplier_name_Field?.dirty || this.supplier_name_Field?.touched);
  }
  get time_per_credit_Field(): AbstractControl | null | undefined{
    return this.form?.get('credit_time');
  }
  get time_per_credit_Valid(){
    return this.time_per_credit_Field?.valid && this.time_per_credit_Field?.dirty;
  }
  get time_per_credit_Invalid(){
    return this.time_per_credit_Field?.invalid && (this.time_per_credit_Field?.dirty || this.time_per_credit_Field?.touched);
  }
  get f(){
    return this.form.controls;   
  }
  get_product_id(){
    let product_id = {
      id: this.product_id
    }
    this.product_service.get_product_id(product_id)
        .subscribe(
          (res: any) => {
            this.product = res.detalle;
            console.log(this.product);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  submit(){
    //COMENTARIO
    this.submitted = true;
    let code_supplier = +this.form.get?.('supplier_code')?.value;

    this.form.get?.('supplier_code')?.setValue(code_supplier);
    
    let form_information = this.form.value;
    
     this.product_service.save_information_analists(form_information)
        .subscribe(
          (res: any) => {
            console.log(res);
            if( res.estado == 1){
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 2500
              }
              );
              let product_information = {
                id: this.product_id,
                user_id: +sessionStorage.getItem('user_id')!,
                estado_id: 3,
              }
              this.product_service.update_states(product_information)
              .subscribe(
                (res: any) => {
                  console.log(res);
                  this.router.navigate(['/dashboard/listAnalists'])
                },
                (error: any) => {
                  console.log(error.error);
                 
                }
              );
            }
            if( res.estado > 1){
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 2500
              }
              );
            }
          },
          (error: any) => {
            console.log(error);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Datos no Registrados! Necesita llenar el Formulario!',
              showConfirmButton: false,
              timer: 2500
            }
            );
          }
        ); 
  }
}
