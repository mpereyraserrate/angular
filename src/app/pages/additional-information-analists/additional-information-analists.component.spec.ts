import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdditionalInformationAnalistsComponent } from './additional-information-analists.component';

describe('AdditionalInformationAnalistsComponent', () => {
  let component: AdditionalInformationAnalistsComponent;
  let fixture: ComponentFixture<AdditionalInformationAnalistsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdditionalInformationAnalistsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdditionalInformationAnalistsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
