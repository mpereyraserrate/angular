import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListProductAnalistBossComponent } from './list-product-analist-boss.component';

describe('ListProductAnalistBossComponent', () => {
  let component: ListProductAnalistBossComponent;
  let fixture: ComponentFixture<ListProductAnalistBossComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListProductAnalistBossComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListProductAnalistBossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
