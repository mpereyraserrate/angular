import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import * as jQuery from 'jquery';
import { CookieService } from 'ngx-cookie-service';
declare var $: any;
@Component({
  selector: 'app-list-product-analist-boss',
  templateUrl: './list-product-analist-boss.component.html',
  styleUrls: ['./list-product-analist-boss.component.css']
})
export class ListProductAnalistBossComponent implements OnInit {
  list_products: Array <any>;
  current_page: number = 1;
  states: any [] = [];
  filterPost: string = "";
  filterPost2: string = "";
  constructor(private router: Router,
              private product_service: ProductService,
              private cookier_service: CookieService) {
    
    this.list_products = [];
    this.states = [];
    console.log(this.list_products);
    this.get_products();
    this.get_states();
}  
view(id: any){
  sessionStorage.setItem('product_id_second_step', id);
  this.router.navigate(['/dashboard/viewBossAnalist']);
}
ngOnInit(): void {

}
get_states(){
  let states = null;
  this.product_service.all_states(states)
      .subscribe(
        (res: any) => {
          this.states = res.detalle;
          console.log(this.states);
        },
        (error: any) => {
          console.log(error.error);
          
        }
      );
}
get_products(){
  var id =  +sessionStorage.getItem('user_id')!;
  let products = {
    estados: '1',
    user_id: 0
  }
  this.product_service.get_all_products2(products)
      .subscribe(
        (res: any) => {
          this.list_products = res.detalle;
          console.log(this.list_products);
        },
        (error: any) => {
          console.log(error.error);
        }
      );
  }
}
