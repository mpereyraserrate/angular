import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-product-list-doctors',
  templateUrl: './product-list-doctors.component.html',
  styleUrls: ['./product-list-doctors.component.css']
})
export class ProductListDoctorsComponent implements OnInit {
  product_id: number;
  products_: Array <any>;
  current_page: number = 1;
  capture: number;
  states: Array <any>;
  filterPost: string = "";
  filterPost2: string = "";
  
  constructor(private product_service: ProductService, private router: Router,
              private cookie_service: CookieService) { 
    this.product_id = 0;
    this.products_ = [];
    this.states = [];
    this.capture = 1;

    this.get_states();
    this.get_all_products();
  }

  ngOnInit(): void {
    this.get_product();
    $(document).on("click",".btnVisualized", function(this: any){
      const modalBg = document.querySelector('#close');
      const modal = document.querySelector('.modal');
     
        modal?.classList.add('is-active');

        modalBg?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
      
    });
  }
  view(id: any){
    sessionStorage.setItem('product_id_view_doctor', id);
    this.router.navigate(['/dashboard/viewDoctors']);
  }
  submit(){

  }
  formPeople(id: any){

  }
  get_product(){
    let product_id = {
      id: 1
    };
    this.product_service.get_product_id(product_id)
        .subscribe(
          (res: any) => {
            console.log(res);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_states(){
    let states = null;
    this.product_service.all_states(states)
        .subscribe(
          (res: any) => {
            this.states = res.detalle;
            console.log(this.states);
          },
          (error: any) => {
            console.log(error.error);
            
          }
        );
  }
  get_all_products(){
    let products = {
      estados: '3',
      user_id: 0,
    }
    this.product_service.get_all_products4(products)
        .subscribe(
          (res: any) => {
            this.products_ = res.detalle;
            console.log(this.products_);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
}
