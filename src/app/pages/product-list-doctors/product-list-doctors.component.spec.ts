import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductListDoctorsComponent } from './product-list-doctors.component';

describe('ProductListDoctorsComponent', () => {
  let component: ProductListDoctorsComponent;
  let fixture: ComponentFixture<ProductListDoctorsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductListDoctorsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListDoctorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
