import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, NgForm  } from '@angular/forms';
import { Router } from '@angular/router';
import { NgSelectConfig } from '@ng-select/ng-select';
import { CookieService } from 'ngx-cookie-service';
import { ProductService } from 'src/app/services/product.service';
@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  @Input() form!: FormGroup;
  submitted: boolean = false;
  step: any = 1;
  dropdownList: any = [];
  product_update_id: any = 0;
  constructor(private router: Router, private fb: FormBuilder, 
              private productService: ProductService,
              private http: HttpClient,
              private cdr: ChangeDetectorRef,
              private config: NgSelectConfig,
              private cookieservice: CookieService) { 
    
    
    this.config.notFoundText = 'Unidad no Encontrada';
    this.dropdownList = this.productService.products;
    this.makeForm();
    this.product_update_id = sessionStorage.getItem('product_update_id');
    console.log(this.product_update_id);
}
  makeForm(){
    this.form = this.fb.group({
      description: ['',[Validators.required,Validators.minLength(5)]],
      container: ['',[Validators.required]],
      pharmaceutical_form: ['', [Validators.required]],
      measure: ['', [Validators.required,Validators.pattern(/^[0-9]\d*$/), Validators.minLength(3)]],
      unit: ['', [Validators.required]],
      precondition_description: ['', [Validators.required]],
      consumption_description: ['', [Validators.required]],
      line: ['', [Validators.required, Validators.minLength(5)]],
      sub_line: ['', [Validators.required, Validators.minLength(5)]],
      group: ['', [Validators.required, Validators.minLength(5)]],
      bar_code_ean_13: ['', [Validators.required, Validators.minLength(5)]],
      bar_code_ean_14: ['', [Validators.required, Validators.minLength(5)]],
      code_ims: ['', [Validators.required, Validators.minLength(5)]],
      code_supplier_invoice: ['', [Validators.required, Validators.minLength(5)]],
      health_register_number: ['', [Validators.required, Validators.minLength(5)]],
      lot_number: ['', [Validators.required, Validators.minLength(5)]],
      product_area: ['', [Validators.required, Validators.minLength(5)]],
      senasag_registration_number: ['', [Validators.required, Validators.minLength(5)]],
      status: ['', [Validators.required, Validators.minLength(5)]],
      validity_initial: ['', [Validators.required, Validators.minLength(5)]],
      validity_final: ['', [Validators.required, Validators.minLength(5)]],
      cold_chain: ['', [Validators.required]],
      humidity: ['', [Validators.required, Validators.minLength(3)]],
      photosensitive: ['', [Validators.required]],
      storage_temperature: ['', [Validators.required, Validators.minLength(3)]],
      storage_position: ['', [Validators.required]],
      unstable: ['', [Validators.required]],
      stackable: ['', [Validators.required]],
      purchase_price: ['', [Validators.required]],
      suggested_price: ['', [Validators.required]],
      commercial_discount: ['', [Validators.required]],
      special_discount: ['', [Validators.required]],

      supplier_name: ['', [Validators.required]],
      fabrication_country: ['', [Validators.required]],
      purchase_unit: ['', [Validators.required]],
      consumption_unit: ['', [Validators.required]],
      embol: ['', [Validators.required]],
      factor_ice: ['', [Validators.required]],
      unit_per_container: ['', [Validators.required]],
      minimum_amount_of_purchase: ['', [Validators.required]],
      unit_per_blister: ['', [Validators.required,Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      relation_blister_box: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      unit_per_package: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      presentation_unit_width: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      presentation_unit_background: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      presentation_unit_height: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      presentation_unit_weight: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      package_width: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      package_background: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      package_height: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      package_weight: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.minLength(3)]],
      active_ingredient: [''],
      concentration: [''],
      presentation: [''],
      therapeutic_action: [''],
      aplications: [''],
      observations: [''],
      file:['']
    });
  }
  ngOnInit(): void {
  }

}
