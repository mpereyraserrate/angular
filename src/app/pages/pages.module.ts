import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { UsersComponent } from './users/users.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { PagesComponent } from './pages.component';
import { StockComponent } from './stock/stock.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddproductsComponent } from './addproducts/addproducts.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { FilterPipe } from '../pipes/filter.pipe';
import { FilterTwoPipe } from '../pipes/filter-two.pipe';
import { ProductListForSupplierComponent } from './product-list-for-supplier/product-list-for-supplier.component';
import { FilterThreePipe } from '../pipes/filter-three.pipe';
import { ProductListDoctorsComponent } from './product-list-doctors/product-list-doctors.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { AdditionalInformationAnalistsComponent } from './additional-information-analists/additional-information-analists.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { ViewProductInformationDoctorsComponent } from './view-product-information-doctors/view-product-information-doctors.component';
import { ListProductAnalistBossComponent } from './list-product-analist-boss/list-product-analist-boss.component';
import { ViewProductAnalistBossComponent } from './view-product-analist-boss/view-product-analist-boss.component';
import { SupplierAssignmentsComponent } from './supplier-assignments/supplier-assignments.component';
import { AnalistsAssignmentsComponent } from "./analists-assignments/analists-assignments.component";
import { FilterFourPipe } from '../pipes/filter-four.pipe';
import { FilterFivePipe } from '../pipes/filter-five.pipe';
import { FilterSixPipe } from '../pipes/filter-six.pipe';
import { FilterSevenPipe } from '../pipes/filter-seven.pipe';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductsComponent,
    UsersComponent,
    PagesComponent,
    StockComponent,
    AddproductsComponent,
    ProductListForSupplierComponent,
    FilterPipe,
    FilterTwoPipe,
    FilterThreePipe,
    FilterFourPipe,
    FilterFivePipe,
    FilterSixPipe,
    FilterSevenPipe,
    ProductListDoctorsComponent,
    AdditionalInformationAnalistsComponent,
    UpdateProductComponent,
    ViewProductInformationDoctorsComponent,
    ListProductAnalistBossComponent,
    ViewProductAnalistBossComponent,
    SupplierAssignmentsComponent,
    AnalistsAssignmentsComponent
  ],
  exports: [
    DashboardComponent,
    ProductsComponent,
    UsersComponent,
    PagesComponent,
    StockComponent,
    AddproductsComponent,
    ProductListForSupplierComponent,
    ProductListDoctorsComponent,
    AdditionalInformationAnalistsComponent,
    UpdateProductComponent,
    ViewProductInformationDoctorsComponent,
    ListProductAnalistBossComponent,
    ViewProductAnalistBossComponent,
    SupplierAssignmentsComponent,
    AnalistsAssignmentsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule,
    NgSelectModule,
    NgMultiSelectDropDownModule.forRoot(),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class PagesModule {}
