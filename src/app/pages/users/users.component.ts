import { Component, OnInit } from '@angular/core';
import * as jQuery from 'jquery';
declare var $: any;
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor() { }
  products: any [] = [
    {
      id: '1',
      product: 'Paracetamol',
      description: 'en proceso'
    },
    {
      id: '2',
      product: 'Aspirina',
      description: 'rechazado'
    },
    {
      id: '3',
      product: 'alcohol',
      description: 'en proceso'
    },
    {
      id: '4',
      product: 'Soda',
      description: 'rechazado'
    },
    {
      id: '5',
      product: 'Galletas',
      description: 'en proceso'
    },
    {
      id: '6',
      product: 'Chicle',
      description: 'aceptado'
    },
    {
      id: '7',
      product: 'Diclofenaco',
      description: 'aceptado'
    },
  ]
  states: any [] = [
    {
      id: 1,
      description: 'en proceso'
    },
    {
      id: 2,
      description: 'aceptado'
    },
    {
      id: 3,
      description: 'rechazado'
    },
  ]
  ngOnInit(): void {
    $(document).ready(function() {
      $('.js-example-basic-single').select2();
    });
  }

}
