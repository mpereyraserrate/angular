import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalistsAssignmentsComponent } from './analists-assignments.component';

describe('AnalistsAssignmentsComponent', () => {
  let component: AnalistsAssignmentsComponent;
  let fixture: ComponentFixture<AnalistsAssignmentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnalistsAssignmentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalistsAssignmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
