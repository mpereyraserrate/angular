import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { NgSelectConfig } from '@ng-select/ng-select';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';
import { AssignmentService } from '../../services/assignment.service';

@Component({
  selector: 'app-analists-assignments',
  templateUrl: './analists-assignments.component.html',
  styleUrls: ['./analists-assignments.component.css']
})
export class AnalistsAssignmentsComponent implements OnInit {
  current_page: number = 1;
  filterPost: string = "";
  filterPost2: string = "";
  filterPost3: string = "";
  filterPost4: string = "";
  filterPost5: string = "";
  list_products: Array <any>;
  states: any [] = [];
  analists: Array <any>;
  lines: Array <any>;
  sub_lines: Array <any>;
  groups: Array <any>;

  analist_code: any;
  line_code: any;
  sub_line_code: any;
  group_code: any;

  analists_assignments: Array <any>;
  constructor(private router: Router, private fb: FormBuilder, 
              private product_service: ProductService,
              private http: HttpClient,
              private cdr: ChangeDetectorRef,
              private config: NgSelectConfig,
              private assignment_service: AssignmentService) { 

    this.list_products = [];
    this.states = [];
    this.analists = [];
    this.lines = [];
    this.sub_lines = [];
    this.groups = [];
    this.analists_assignments = [];
    this.config.notFoundText = 'Dato no Encontrado';
    this.get_products();
    this.get_states();   
    this.get_analists();   
    this.get_line(); 
    this.get_sub_line();  
    this.get_group();
    this.get_analists_assignments();
}

  ngOnInit(): void {
    $(document).on("click",".btnEdit", function(this: any){
      const modalBg = document.querySelector('#close');
      const modal = document.querySelector('.modal');
      const modalBg2 = document.querySelector('.close2');

        modal?.classList.add('is-active');

        modalBg?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
        modalBg2?.addEventListener('click', () =>{
          
          modal?.classList.remove('is-active');
            
        });
      });
  }
  get_states(){
    let states = null;
    this.product_service.all_states(states)
        .subscribe(
          (res: any) => {
            this.states = res.detalle;
            console.log(this.states);
          },
          (error: any) => {
            console.log(error.error);
            
          }
        );
  }
  get_products(){
    var id =  +sessionStorage.getItem('user_id')!;
    let products = {
      estados: '1,2,3,4,5',
      user_id: id,
    }
    this.product_service.get_all_products2(products)
        .subscribe(
          (res: any) => {
            this.list_products = res.detalle;
            console.log(this.list_products);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  get_analists(){
    let analists = {
      codigoperfil: 1003,
      estado: 1,
    }
    this.assignment_service.get_perfil_users(analists)
        .subscribe(
          (res: any) => {
            this.analists = res.detalle;
            console.log(this.analists);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_line(){
    let line = {
      codigoLinea: 0
    }
    this.assignment_service.get_lines(line)
        .subscribe(
          (res: any) => {
            this.lines = res.detalle;
            console.log(this.lines);
            
          },
          (error: any) => {
            console.log(error);
            
          }
        );
  }
  get_sub_line(){
    let sub_line = {
      codigosubLinea: 0,
    }
    this.assignment_service.get_sub_lines(sub_line)
        .subscribe(
          (res: any) => {
            this.sub_lines = res.detalle
            console.log(this.sub_lines);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_group(){
    let groups = {
      codigogrupo: this.group_code,
    }
    this.assignment_service.get_groups(groups)
        .subscribe(
          (res: any) => {
            this.groups = res.detalle;
            console.log(this.groups);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  save_assignment(){
    let information = {
      codigoanalista: this.analist_code,
      codigolinea: this.line_code,
      codigosublinea: this.sub_line_code,
      codigogrupo: this.group_code,
      estado: 1,
      user_id: +sessionStorage.getItem('user_id')!,
    }
    this.assignment_service.save_analist_assignments(information)
        .subscribe(
          (res: any) => {
            console.log(res);
            if(res.estado == 1){
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 1500
              }
              );
            }else{
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 1500
              }
              );
            }
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  get_analists_assignments(){
    let information = {
      codigoanalista: 0,
    }
    this.assignment_service.get_analists_assignments(information)
        .subscribe(
          (res: any) => {
            this.analists_assignments = res.detalle;

            console.log(this.analists_assignments);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
}
