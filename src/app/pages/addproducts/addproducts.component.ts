import { group } from '@angular/animations';
import { Component, OnInit, ChangeDetectorRef, AfterContentChecked, enableProdMode, Pipe } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { FormValidations } from '../../validations/form.validations';
import { HttpClient } from '@angular/common/http';
import * as jQuery from 'jquery';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { invalid } from '@angular/compiler/src/render3/view/util';
import Swal from 'sweetalert2';
import { validateHorizontalPosition } from '@angular/cdk/overlay';
/* import * as EventEmitter from 'events'; */
import { Output } from '@angular/core';
import { catchError, map, throwIfEmpty } from 'rxjs/operators';
import { NgSelectConfig } from '@ng-select/ng-select';
import { CookieService } from 'ngx-cookie-service';
import { throwError } from 'rxjs';
import { AssignmentService } from '../../services/assignment.service';

declare var $: any;

@Component({
  selector: 'app-addproducts',
  templateUrl: './addproducts.component.html',
  styleUrls: ['./addproducts.component.css']
})
export class AddproductsComponent implements OnInit, AfterContentChecked {
  dropdownList: any = [];
  selectedItems: string = "";
  dropdownSettings = {};
  form!: FormGroup;
  submitted: boolean = false;
  step: any = 1;
  container: Array <any>;
  pharmaceutical: Array <any>;
  units: Array <any>;
  preconditions: Array <any>;
  types: Array <any>;
  user_id: any = 0;
  information: Array <any>;
  
  selectedFile: any;
  estado: string = "Pendiente"
  constructor( private router: Router, private fb: FormBuilder, 
               private productService: ProductService,
               private http: HttpClient,
               private cdr: ChangeDetectorRef,
               private config: NgSelectConfig,
               private cookieservice: CookieService,
               private assignment_service: AssignmentService) { 
    this.user_id = +sessionStorage.getItem('user_id')!;
    this.container = [];
    this.pharmaceutical = [];
    this.units = [];
    this.preconditions = [];
    this.types = [];
    this.information = [];
    this.config.notFoundText = 'Dato no Encontrado';
    this.dropdownList = this.productService.products;

    this.makeForm();
    this.list_container();
    this.list_pharmaceutical();
    this.list_unit();
    this.list_precondition();
    this.list_type();
    this.get_assignments();
  }

  ngOnInit(): void {
   /* jQuery('#next1').click(
      function(e){
       // alert('hola next1');
        
       //var child = $("form").children('.container.mt-2');
       //child.hide();
        $("#form1").hide();
        $("#form2").hide();
        $("#form3").hide();
        
        var id = $(e.target);
        var name = "#" + id.attr('name');
        //alert("#" + id.attr('name'));
        //id.next.show();
        //$(name).next.show();

        var bro = $("form").next('.hidden');
        var namebro =  bro.attr('name');
       // alert("#" + bro.attr('name'));
        $("#"+ namebro).show();
        //$("#"+ namebro).fadeIn(3000);
        
        
        //form2.style.left = "40px";
      });
*/
      jQuery(document).on("click", "button.btnnext", function (e) {
        //jQuery("#ajax-loader-element").show();
        //alert("hizo click");
        $("#form1").hide();
        $("#form2").hide();
        $("#form3").hide();
        
        var id = $(e.target);
        var name = "#" + id.attr('name');
        var progress = '#' + id.attr('progress');
        //alert("#" + id.attr('name'));
        //id.next.show();
        //$(name).next.show();
       
        /* alert("id#" + id.attr('name')); */

       // id.attr('name')
       var $currDiv = $(name);
       /* alert($currDiv); */
       /* alert($currDiv.attr('name')); */
       var $currBro  =  $currDiv .next();
       /* alert($currBro.attr('name')); */

        var bro = $("form").next('.hidden');
        var namebro =  bro.attr('name');
       /* alert("bro#" + bro.attr('name')); */
        $("#"+ namebro).show();
        //$("#"+ namebro).fadeIn(3000);
        
    });

    


  }
  
  makeForm(){
    this.form = this.fb.group({
      description: ['',[Validators.required,Validators.maxLength(25)], FormValidations.validateProduct(this.productService)],
      container: ['',[Validators.required]],
      pharmaceutical_form: ['', [Validators.required]],
      measure: ['', [Validators.required,Validators.pattern(/^[0-9]\d*$/)]],
      unit: ['', [Validators.required]],
      precondition_for_sale: ['', [Validators.required]],
      consumption_description: ['', [Validators.required]],
      line_id: ['', [Validators.required]],
      sub_line_id: ['', [Validators.required]],
      group_id: ['', [Validators.required]],
      bar_code_ean_13: ['', [Validators.required, Validators.maxLength(25)]],
      bar_code_ean_14: ['', [Validators.required, Validators.maxLength(25)]],
      code_ims: ['', [Validators.required, Validators.maxLength(25)]],
      code_supplier_invoice: ['', [Validators.required,,Validators.max(9999999999999999999999999)]],
      health_register_number: ['', [Validators.required, Validators.maxLength(25)]],
      lot_number: ['', [Validators.required,,Validators.max(9999999999999999999999999)]],
      product_area: ['', [Validators.required, Validators.maxLength(25)]],
      senasag_registration_number: ['', [Validators.required, Validators.maxLength(25)]],
      status: ['Vigente'],
      validity_initial: ['', [Validators.required, Validators.maxLength(25)]],
      validity_final: ['', [Validators.required, Validators.maxLength(25)]],
      cold_chain: ['', [Validators.required]],
      humidity: ['', [Validators.required, Validators.maxLength(25)]],
      photosensitive: ['', [Validators.required]],
      storage_temperature: ['', [Validators.required, Validators.maxLength(25)]],
      storage_position: ['', [Validators.required]],
      unstable: ['', [Validators.required]],
      stackable: ['', [Validators.required]],
      purchase_price: ['', [Validators.required,Validators.max(999)]],
      suggested_price: ['', [Validators.required,Validators.max(999)]],
      commercial_discount: ['', [Validators.required,Validators.max(999)]],
      special_discount: ['', [Validators.required,Validators.max(999)]],
      supplier_name: ['', [Validators.required]],
      fabrication_country: ['', [Validators.required]],
      purchase_unit: ['', [Validators.required]],
      consumption_unit: ['', [Validators.required]],
      embol: [''],
      factor_ice: ['', [Validators.required]],
      unit_per_container: ['', [Validators.required,Validators.max(999)]],
      minimum_amount_of_purchase: ['', [Validators.required]],
      unit_per_blister: [''],
      relation_blister_box: [''/* Validators.pattern(/^[0-9]\d*$/) */],
      unit_per_package: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      presentation_unit_width: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      presentation_unit_background: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      presentation_unit_height: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      presentation_unit_weight: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      package_width: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      package_background: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      package_height: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      package_weight: ['', [Validators.required, Validators.pattern(/^[0-9]\d*$/),Validators.max(999)]],
      active_ingredient: [''],
      concentration: [''],
      presentation: [''],
      therapeutic_action: [''],
      aplications: [''],
      observations: [''],
      file: null,
      user_id:[this.user_id],
      estado: [this.estado],
      estado_id: 1,
    });
  }
  get descriptionField(): AbstractControl | null | undefined {
    return this.form?.get('description');
  }
  get descriptionValid(){
    return this.descriptionField?.valid;
  }
  get descriptionInvalid(){
    return  this.descriptionField?.invalid && this.descriptionField?.dirty;
  }
  get container_Field(): AbstractControl | null | undefined{
    return this.form?.get('container');
  }
  get container_Valid(){
    return this.container_Field?.valid
  }
  get container_Invalid(){
    return this.container_Field?.invalid && this.container_Field?.dirty
  }
  get pharmaceutical_form_Field(): AbstractControl | null | undefined{
    return this.form?.get('pharmaceutical_form');
  }
  get pharmaceutical_form_Valid(){
    return this.pharmaceutical_form_Field?.valid;
  }
  get pharmaceutical_form_Invalid(){
    return this.pharmaceutical_form_Field?.invalid && this.pharmaceutical_form_Field?.dirty;
  }
  get measure_Field(): AbstractControl | null | undefined{
    return this.form?.get('measure');
  }
  get measure_Valid(){
    return this.measure_Field?.valid
  }
  get measure_Invalid(){
    return this.measure_Field?.invalid && this.measure_Field?.dirty
  }
  get unit_Field(): AbstractControl | null | undefined{
    return this.form?.get('unit');
  }
  get unit_Valid(){
    return this.unit_Field?.valid
  }
  get unit_Invalid(){
    return this.unit_Field?.invalid && this.unit_Field?.dirty
  }
  get precondition_description_Field(): AbstractControl | null | undefined{
    return this.form?.get('precondition_for_sale');
  }
  get precondition_description_Valid(){
    return this.precondition_description_Field?.valid
  }
  get precondition_description_Invalid(){
    return this.precondition_description_Field?.invalid && this.precondition_description_Field?.dirty
  }
  get consumption_description_Field(): AbstractControl | null | undefined{
    return this.form?.get('consumption_description');
  }
  get consumption_description_Valid(){
    return this.consumption_description_Field?.valid
  }
  get consumption_description_Invalid(){
    return this.consumption_description_Field?.invalid && this.consumption_description_Field?.dirty
  }
  get line_Field(): AbstractControl | null | undefined{
    return this.form?.get('line_id');
  }
  get line_Valid(){
    return this.line_Field?.valid;
  }
  get line_Invalid(){
    return this.line_Field?.invalid && this.line_Field?.dirty;
  }
  get sub_line_Field(): AbstractControl | null | undefined{
    return this.form?.get('sub_line_id');
  }
  get sub_line_Valid(){
    return this.sub_line_Field?.valid;
  }
  get sub_line_Invalid(){
    return this.sub_line_Field?.invalid && this.sub_line_Field?.dirty;
  }
  get group_Field(): AbstractControl | null | undefined{
    return this.form?.get('group_id');
  }
  get group_Valid(){
    return this.group_Field?.valid;
  }
  get group_Invalid(){
    return this.group_Field?.invalid && this.group_Field?.dirty;
  }
  get bar_code_ean_13_Field(): AbstractControl | null | undefined{
    return this.form?.get('bar_code_ean_13');
  }
  get bar_code_ean_13_Valid(){
    return  this.bar_code_ean_13_Field?.valid
  }
  get bar_code_ean_13_Invalid(){
    return this.bar_code_ean_13_Field?.invalid && this.bar_code_ean_13_Field?.dirty
  }
  get bar_code_ean_14_Field(): AbstractControl | null | undefined{
    return this.form?.get('bar_code_ean_14');
  }
  get bar_code_ean_14_Valid(){
    return this.bar_code_ean_14_Field?.valid
  }
  get bar_code_ean_14_Invalid(){
    return this.bar_code_ean_14_Field?.invalid && this.bar_code_ean_14_Field?.dirty
  }
  get code_ims_Field(): AbstractControl | null | undefined{
    return this.form?.get('code_ims');
  }
  get code_ims_Valid(){
    return this.code_ims_Field?.valid
  }
  get code_ims_Invalid(){
    return this.code_ims_Field?.invalid && this.code_ims_Field?.dirty
  }
  get code_supplier_invoice_Field(): AbstractControl | null | undefined{
    return this.form?.get('code_supplier_invoice');
  }
  get code_supplier_invoice_Valid(){
    return this.code_supplier_invoice_Field?.valid
  }
  get code_supplier_invoice_Invalid(){
    return this.code_supplier_invoice_Field?.invalid && this.code_supplier_invoice_Field?.dirty
  }
  get health_register_number_Field(): AbstractControl | null | undefined{
    return this.form?.get('health_register_number');
  }
  get health_register_number_Valid(){
    return this.health_register_number_Field?.valid
  }
  get health_register_number_Invalid(){
    return this.health_register_number_Field?.invalid && this.health_register_number_Field.dirty;
  }
  get lot_number_Field(): AbstractControl | null | undefined{
    return this.form?.get('lot_number');
  }
  get lot_number_Valid(){
    return this.lot_number_Field?.valid
  }
  get lot_number_Invalid(){
    return this.lot_number_Field?.invalid && this.lot_number_Field?.dirty
  }
  
  get product_area_Field(): AbstractControl | null | undefined{
    return this.form?.get('product_area');
  }
  get product_area_Valid(){
    return this.product_area_Field?.valid
  }
  get product_area_Invalid(){
    return this.product_area_Field?.invalid && this.product_area_Field?.dirty
  }
  get senasag_registration_number_Field(): AbstractControl | null | undefined{
    return this.form?.get('senasag_registration_number');
  }
  get senasag_registration_number_Valid(){
    return this.senasag_registration_number_Field?.valid
  }
  get senasag_registration_number_Invalid(){
    return this.senasag_registration_number_Field?.invalid && this.senasag_registration_number_Field?.dirty
  }
  get status_Field(): AbstractControl | null | undefined{
    return this.form?.get('status');
  }
  get status_Valid(){
    return this.status_Field?.valid
  }
  get status_Invalid(){
    return this.status_Field?.invalid && this.status_Field?.dirty
  }
  get validity_initial_Field(): AbstractControl | null | undefined{
    return this.form?.get('validity_initial');
  }
  get validity_initial_Valid(){
    return this.validity_initial_Field?.valid
  }
  get validity_initial_Invalid(){
    return this.validity_initial_Field?.invalid && this.validity_initial_Field?.dirty
  }
  get validity_final_Field(): AbstractControl | null | undefined{
    return this.form?.get('validity_final');
  }
  get validity_final_Valid(){
    return this.validity_final_Field?.valid
  }
  get validity_final_Invalid(){
    return this.validity_final_Field?.invalid && this.validity_final_Field?.dirty
  }
  get cold_chain_Field(): AbstractControl | null | undefined{
    return this.form?.get('cold_chain');
  }
  get cold_chain_Valid(){
    return this.cold_chain_Field?.valid;
  }
  get cold_chain_Invalid(){
    return this.cold_chain_Field?.invalid && this.cold_chain_Field?.dirty;
  }
  get humidity_Field(): AbstractControl | null | undefined{
    return this.form?.get('humidity');
  }
  get humidity_Valid(){
    return this.humidity_Field?.valid;
  }
  get humidity_Invalid(){
    return this.humidity_Field?.invalid && this.humidity_Field?.dirty;
  }
  get photosensitive_Field(): AbstractControl | null | undefined{
    return this.form?.get('photosensitive');
  }
  get photosensitive_Valid(){
    return this.photosensitive_Field?.valid;
  }
  get photosensitive_Invalid(){
    return this.photosensitive_Field?.invalid && this.photosensitive_Field?.dirty;
  }
  get storage_temperature_Field(): AbstractControl | null | undefined{
    return this.form?.get('storage_temperature');
  }
  get storage_temperature_Valid(){
    return this.storage_temperature_Field?.valid;
  }
  get storage_temperature_Invalid(){
    return this.storage_temperature_Field?.invalid && this.storage_temperature_Field?.dirty;
  }
  get storage_position_Field(): AbstractControl | null | undefined{
    return this.form?.get('storage_position');
  }
  get storage_position_Valid(){
    return this.storage_position_Field?.valid;
  }
  get storage_position_Invalid(){
    return this.storage_position_Field?.invalid && this.storage_position_Field?.dirty;
  }
  get unstable_Field(): AbstractControl | null | undefined{
    return this.form?.get('unstable');
  }
  get unstable_Valid(){
    return this.unstable_Field?.valid;
  }
  get unstable_Invalid(){
    return this.unstable_Field?.invalid && this.unstable_Field?.dirty;
  }
  get stackable_Field(): AbstractControl | null | undefined{
    return this.form?.get('stackable');
  }
  get stackable_Valid(){
    return this.stackable_Field?.valid;
  }
  get stackable_Invalid(){
    return this.stackable_Field?.invalid && this.stackable_Field?.dirty;
  }
  




  get purchase_price_Field(): AbstractControl | null | undefined{
    return this.form?.get('purchase_price');
  }
  get purchase_price_Valid(){
    return this.purchase_price_Field?.valid;
  }
  get purchase_price_Invalid(){
    return this.purchase_price_Field?.invalid && this.purchase_price_Field?.dirty;
  }
  get suggested_price_Field(): AbstractControl | null | undefined{
    return this.form?.get('suggested_price');
  }
  get suggested_price_Valid(){
    return this.suggested_price_Field?.valid;
  }
  get suggested_price_Invalid(){
    return this.suggested_price_Field?.invalid && this.suggested_price_Field?.dirty;
  }
  get commercial_discount_Field(): AbstractControl | null | undefined{
    return this.form?.get('commercial_discount');
  }
  get commercial_discount_Valid(){
    return this.commercial_discount_Field?.valid;
  }
  get commercial_discount_Invalid(){
    return this.commercial_discount_Field?.invalid && this.commercial_discount_Field?.dirty;
  }
  get special_discount_Field(): AbstractControl | null | undefined{
    return this.form?.get('special_discount');
  }
  get special_discount_Valid(){
    return this.special_discount_Field?.valid;
  }
  get special_discount_Invalid(){
    return this.special_discount_Field?.invalid && this.special_discount_Field?.dirty;
  }
  get supplier_name_Field(): AbstractControl | null | undefined{
    return this.form?.get('supplier_name');
  }
  get supplier_name_Valid(){
    return this.supplier_name_Field?.valid;
  }
  get supplier_name_Invalid(){
    return this.supplier_name_Field?.invalid && this.supplier_name_Field?.dirty;
  }
  get fabrication_country_Field(): AbstractControl | null | undefined{
    return this.form?.get('fabrication_country');
  }
  get fabrication_country_Valid(){
    return this.fabrication_country_Field?.valid;
  }
  get fabrication_country_Invalid(){
    return this.fabrication_country_Field?.invalid && this.fabrication_country_Field?.dirty;
  }
  get purchase_unit_Field(): AbstractControl | null | undefined{
    return this.form?.get('purchase_unit');
  }
  get purchase_unit_Valid(){
    return this.purchase_unit_Field?.valid;
  }
  get purchase_unit_Invalid(){
    return this.purchase_unit_Field?.invalid && this.purchase_unit_Field?.dirty;
  }
  get consumption_unit_Field(): AbstractControl | null | undefined{
    return this.form?.get('consumption_unit');
  }
  get consumption_unit_Valid(){
    return this.consumption_unit_Field?.valid;
  }
  get consumption_unit_Invalid(){
    return this.consumption_unit_Field?.invalid && this.consumption_unit_Field?.dirty;
  }
  get embol_Field(): AbstractControl | null | undefined{
    return this.form?.get('embol');
  }
  get embol_Valid(){
    return this.embol_Field?.valid && this.embol_Field?.dirty;
  }
  /* embol_Invalid(){
    if(this.form?.get('embol')?.value == ""){
      return true;
    }
  } */
  /* get embol_Invalid(){
    return this.embol_Field?.invalid && this.embol_Field?.dirty;
  } */
  get factor_ice_Field(): AbstractControl | null | undefined{
    return this.form?.get('factor_ice');
  }
  get factor_ice_Valid(){
    return this.factor_ice_Field?.valid;
  }
  get factor_ice_Invalid(){
    return this.factor_ice_Field?.invalid && this.factor_ice_Field?.dirty;
  }
  get unit_per_container_Field(): AbstractControl | null | undefined{
    return this.form?.get('unit_per_container');
  }
  get unit_per_container_Valid(){
    return this.unit_per_container_Field?.valid;
  }
  get unit_per_container_Invalid(){
    return this.unit_per_container_Field?.invalid && this.unit_per_container_Field?.dirty;
  }
  get minimum_amount_of_purchase_Field(): AbstractControl | null | undefined{
    return this.form?.get('minimum_amount_of_purchase');
  }
  get minimum_amount_of_purchase_Valid(){
    return this.minimum_amount_of_purchase_Field?.valid;
  }
  get minimum_amount_of_purchase_Invalid(){
    return this.minimum_amount_of_purchase_Field?.invalid && this.minimum_amount_of_purchase_Field?.dirty;
  }
  get unit_per_blister_Field(): AbstractControl | null | undefined{
    return this.form?.get('unit_per_blister');
  }
  get unit_per_blister_Valid(){
    return this.unit_per_blister_Field?.valid && this.unit_per_blister_Field?.dirty;
  }
  get unit_per_blister_Invalid(){
    return this.unit_per_blister_Field?.invalid && this.unit_per_blister_Field?.dirty;
  }
  get relation_blister_box_Field(): AbstractControl | null | undefined{
    return this.form?.get('relation_blister_box');
  }
  get relation_blister_box_Valid(){
    return this.relation_blister_box_Field?.valid && this.relation_blister_box_Field?.dirty;
  }
  get relation_blister_box_Invalid(){
    return this.relation_blister_box_Field?.invalid && this.relation_blister_box_Field?.dirty;
  }
  get unit_per_package_Field(): AbstractControl | null | undefined{
    return this.form?.get('unit_per_package');
  }
  get unit_per_package_Valid(){
    return this.unit_per_package_Field?.valid;
  }
  get unit_per_package_Invalid(){
    return this.unit_per_package_Field?.invalid && this.unit_per_package_Field?.dirty;
  }
  get presentation_unit_width_Field(): AbstractControl | null | undefined{
    return this.form?.get('presentation_unit_width');
  }
  get presentation_unit_width_Valid(){
    return this.presentation_unit_width_Field?.valid;
  }
  get presentation_unit_width_Invalid(){
    return this.presentation_unit_width_Field?.invalid && this.presentation_unit_width_Field?.dirty;
  }
  get presentation_unit_background_Field(): AbstractControl | null | undefined{
    return this.form?.get('presentation_unit_background');
  }
  get presentation_unit_background_Valid(){
    return this.presentation_unit_background_Field?.valid;
  }
  get presentation_unit_background_Invalid(){
    return this.presentation_unit_background_Field?.invalid && this.presentation_unit_background_Field?.dirty;
  }
  get presentation_unit_height_Field(): AbstractControl | null | undefined{
    return this.form?.get('presentation_unit_height');
  }
  get presentation_unit_height_Valid(){
    return this.presentation_unit_height_Field?.valid;
  }
  get presentation_unit_height_Invalid(){
    return this.presentation_unit_height_Field?.invalid && this.presentation_unit_height_Field?.dirty;
  }
  get presentation_unit_weight_Field(): AbstractControl | null | undefined{
    return this.form?.get('presentation_unit_weight');
  }
  get presentation_unit_weight_Valid(){
    return this.presentation_unit_weight_Field?.valid;
  }
  get presentation_unit_weight_Invalid(){
    return this.presentation_unit_weight_Field?.invalid && this.presentation_unit_weight_Field?.dirty;
  }
  get package_width_Field(): AbstractControl | null | undefined{
    return this.form?.get('package_width');
  }
  get package_width_Valid(){
    return this.package_width_Field?.valid;
  }
  get package_width_Invalid(){
    return this.package_width_Field?.invalid && this.package_width_Field?.dirty;
  }
  get package_background_Field(): AbstractControl | null | undefined{
    return this.form?.get('package_background');
  }
  get package_background_Valid(){
    return this.package_background_Field?.valid;
  }
  get package_background_Invalid(){
    return this.package_background_Field?.invalid && this.package_background_Field?.dirty;
  }
  get package_height_Field(): AbstractControl | null | undefined{
    return this.form?.get('package_height');
  }
  get package_height_Valid(){
    return this.package_height_Field?.valid;
  }
  get package_height_Invalid(){
    return this.package_height_Field?.invalid && this.package_height_Field?.dirty;
  }
  get package_weight_Field(): AbstractControl | null | undefined{
    return this.form?.get('package_weight');
  }
  get package_weight_Valid(){
    return this.package_weight_Field?.valid;
  }
  get package_weight_Invalid(){
    return this.package_weight_Field?.invalid && this.package_weight_Field?.dirty;
  }
  get active_ingredient_Field(): AbstractControl | null | undefined{
    return this.form?.get('active_ingredient');
  }
  get active_ingredient_Valid(){
    return this.active_ingredient_Field?.valid && this.active_ingredient_Field?.dirty;
  }
  get concentration_Field(): AbstractControl | null | undefined{
    return this.form?.get('concentration');
  }
  get concentration_Valid(){
    return this.concentration_Field?.valid && this.concentration_Field?.dirty;
  }
  get presentation_Field(): AbstractControl | null | undefined{
    return this.form?.get('presentation');
  }
  get presentation_Valid(){
    return this.presentation_Field?.valid && this.presentation_Field?.dirty;
  }
  get therapeutic_action_Field(): AbstractControl | null | undefined{
    return this.form?.get('therapeutic_action');
  }
  get therapeutic_action_Valid(){
    return this.therapeutic_action_Field?.valid && this.therapeutic_action_Field?.dirty;
  }
  get aplications_Field(): AbstractControl | null | undefined{
    return this.form?.get('aplications');
  }
  get aplications_Valid(){
    return this.aplications_Field?.valid && this.aplications_Field?.dirty;
  }
  get observations_Field(): AbstractControl | null | undefined{
    return this.form?.get('observations');
  }
  get observations_Valid(){
    return this.observations_Field?.valid && this.observations_Field?.dirty;
  }
  ngAfterContentChecked(): void {
    this.cdr.detectChanges();
  }
  get f(){
    return this.form.controls;   
  }
  next_step(){
    this.submitted = true;
    if(this.descriptionValid && this.container_Valid && this.pharmaceutical_form_Valid && this.measure_Valid &&
       this.unit_Valid && this.precondition_description_Valid && this.consumption_description_Valid &&
        this.line_Valid && this.sub_line_Valid && this.group_Valid && this.bar_code_ean_13_Valid && this.bar_code_ean_14_Valid){
      this.step = this.step + 1;
      console.log(this.step);
      return this.step;
    }else{
      if(this.form.invalid){
        return
      }
    }
  }
  next_step2(){
    this.submitted = true;
    if(this.code_ims_Valid && this.code_supplier_invoice_Valid && this.senasag_registration_number_Valid && this.lot_number_Valid &&
       this.product_area_Valid && this.health_register_number_Valid && this.status_Valid && this.validity_initial_Valid && this.validity_final_Valid){
      this.step = this.step + 1;
      console.log(this.step);
      return this.step;
    }else{
      if(this.form.invalid){
        return
      }
    }
  }
  next_step3(){
    this.submitted = true;
    if(this.cold_chain_Valid && this.humidity_Valid && this.photosensitive_Valid && this.storage_temperature_Valid &&
       this.storage_position_Valid && this.unstable_Valid && this.stackable_Valid){
      this.step = this.step + 1;
      console.log(this.step);
      return this.step;
    }else{
      if(this.form.invalid){
        return
      }
    }
  }
  next_step4(){
    this.submitted = true;
    if(this.purchase_price_Valid && this.suggested_price_Valid && this.commercial_discount_Valid && this.special_discount_Valid &&
       this.supplier_name_Valid && this.fabrication_country_Valid && this.purchase_unit_Valid && this.consumption_unit_Valid &&
       this.factor_ice_Valid && this.unit_per_container_Valid && this.minimum_amount_of_purchase_Valid){
      this.step = this.step + 1;
      
      return this.step
    }else{
      if(this.form.invalid){
        return
      }
    }
  }
  next_step5(){
    this.submitted = true;
    if(this.unit_per_package_Valid && this.presentation_unit_width_Valid &&
       this.presentation_unit_background_Valid && this.presentation_unit_height_Valid && this.presentation_unit_weight_Valid &&
       this.package_width_Valid && this.package_background_Valid && this.package_height_Valid && this.package_weight_Valid){
      this.step = this.step + 1;
      return this.step;
    }else{
      if(this.form.invalid){
        return   
      }
    }
  }
  next_step6(){
    this.step = this.step + 1;
    return this.step;
  }
  previous(){
    this.step = this.step - 1;
    return this.step;
  }
  /* onFileSelected(event: any){
    const file2= event.target.files[0];
    this.form.get('file')?.setValue(file2);
    console.log(file2);
    console.log(this.form.get('file')?.value);
    
  } */
  onFileSelected(event: any){
      /* console.log(event); */
      this.selectedFile = event.target.files[0];
  }
  submit(){
    let cold_chain = JSON.parse(this.form.get('cold_chain')?.value);
    this.form.get('cold_chain')?.setValue(cold_chain);

    let photosensitive = JSON.parse(this.form.get('photosensitive')?.value);
    this.form.get('photosensitive')?.setValue(photosensitive);

    let unstable = JSON.parse(this.form.get('unstable')?.value);
    this.form.get('unstable')?.setValue(unstable);

    let stackable = JSON.parse(this.form.get('stackable')?.value);
    this.form.get('stackable')?.setValue(stackable);
     
    
    const fileData = new FormData();
        fileData.append('image', this.selectedFile);
    

    let product_form = this.form.value;
    
    this.productService.save_product(product_form)
        .subscribe(
          (res: any) => {
            console.log(res);
            if(res.estado == 1){
              Swal.fire({
                position: 'top-end',
                icon: 'success',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 2000
              }
              );
            }
            if(res.estado == 2){
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 2000
              }
              );
            }
            if(res.estado == 3){
              Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: res.mensaje,
                showConfirmButton: false,
                timer: 2000
              }
              );
            }
          },
          (error: any) => {
            console.log(error.error);
          }
        ); 
  }
  /* ngAfterViewInit() {
    throw new Error('Function not implemented.');
  } */
  list_container(){
    let user = null;
    this.productService.get_list_container(user)
        .subscribe(
          (res: any) => {
            this.container = res.detalle;
            console.log(this.container);
          },
          (error: any) =>{
            console.log(error.error);
            
          }
        )
  }
  list_pharmaceutical(){
    let user = null;
    this.productService.get_list_pharmaceutical(user)
        .subscribe(
          (res: any) => {
            this.pharmaceutical = res.detalle;
            console.log(this.pharmaceutical);
          },
          (error: any) => {
            console.log(error.error);
          }
        );   
  }    
  list_unit(){
    let user = null;
    this.productService.get_list_unit(user)
        .subscribe(
          (res: any) => {
            this.units = res.detalle;
            console.log(this.units);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  } 
  list_precondition(){
    let user = null;
    this.productService.get_list_precondition(user)
        .subscribe(
          (res: any) => {
            this.preconditions = res.detalle;
            console.log(this.preconditions);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }  
  list_type(){
    let user = null;
    this.productService.get_list_consumption(user)
        .subscribe(
          (res: any) => {
            this.types = res.detalle;
            console.log(this.types);
          },
          (error: any) => {
            console.log(error.error);
          }
        );
  }
  get_assignments(){
    let information = {
      codigoproveedor: +sessionStorage.getItem('user_id')!,
    }
    this.assignment_service.get_supplier_assignments(information)
        .subscribe(
          (res: any) => {
            this.information = res.detalle;
            console.log(this.information);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
}




