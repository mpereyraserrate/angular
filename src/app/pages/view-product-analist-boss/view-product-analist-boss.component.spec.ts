import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProductAnalistBossComponent } from './view-product-analist-boss.component';

describe('ViewProductAnalistBossComponent', () => {
  let component: ViewProductAnalistBossComponent;
  let fixture: ComponentFixture<ViewProductAnalistBossComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewProductAnalistBossComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProductAnalistBossComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
