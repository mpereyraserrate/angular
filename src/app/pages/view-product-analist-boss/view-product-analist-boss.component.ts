import { Component, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-product-analist-boss',
  templateUrl: './view-product-analist-boss.component.html',
  styleUrls: ['./view-product-analist-boss.component.css']
})
export class ViewProductAnalistBossComponent implements OnInit {
  public analist_selected: string = "";
  public observations: string = "";

  product: Array <any>;
  product_id: any  = 0;
  constructor(private router: Router,
              private product_service:ProductService,
              private cookie_service:CookieService) {
      this.product = [];
      this.product_id =  +sessionStorage.getItem('product_id_second_step')!;
      console.log(this.product_id);
      
      this.get_product_id();
  }

  ngOnInit(): void {
    jQuery(document).on('click', '#product_information1',function(){
      const form1 = document.querySelector('#form1');
      const form2 = document.querySelector('.form2');
      const form3 = document.querySelector('.form3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');


      form1?.classList.remove('is-hidden');
      form2?.classList.add('is-hidden');
      form3?.classList.add('is-hidden');
      tab1?.classList.add('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information2',function(){
      const form1 = document.querySelector('#form1');
      const form2 = document.querySelector('.form2');
      const form3 = document.querySelector('.form3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');

      form1?.classList.add('is-hidden');
      form2?.classList.remove('is-hidden');
      form3?.classList.add('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.add('is-active');
      tab3?.classList.remove('is-active');
    });
    jQuery(document).on('click', '#product_information3',function(){
      const form1 = document.querySelector('#form1');
      const form2 = document.querySelector('.form2');
      const form3 = document.querySelector('.form3');
      const tab1 = document.querySelector('.tab1');
      const tab2 = document.querySelector('.tab2');
      const tab3 = document.querySelector('.tab3');

      form1?.classList.add('is-hidden');
      form2?.classList.add('is-hidden');
      form3?.classList.remove('is-hidden');
      tab1?.classList.remove('is-active');
      tab2?.classList.remove('is-active');
      tab3?.classList.add('is-active');
    });
  }
  get_product_id(){
    let product_id = {
      id: this.product_id,
    }
    this.product_service.get_product_id(product_id)
        .subscribe(
          (res: any) => {
            this.product = res.detalle;
            console.log(this.product);
          },
          (error: any) => {
            console.log(error);
          }
        );
  }
  approve(product_id: any){
    let product_information = {
      id: product_id,
      user_id: +sessionStorage.getItem('user_id')!,
      estado_id: 2,
    }
    this.product_service.update_states(product_information)
        .subscribe(
          (res: any) => {
            console.log(res);
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: res.mensaje,
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/listBossAnalist'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500); */
          },
          (error: any) => {
            console.log(error.error);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Error al Actualizar el Estado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/listBossAnalist'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500) */
          }
        );
  }
  disapprove(product_id: any){
    let product_information = {
      id: product_id,
      user_id: +sessionStorage.getItem('user_id')!,
      estado_id: 5,
    }
    this.product_service.update_states(product_information)
        .subscribe(
          (res: any) => {
            /* console.log(res); */
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: res.mensaje + 'Producto Rechazado para Completado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/listBossAnalist'])
                        /* setTimeout(() => {
                          window.location.reload();
                        }, 2500) */
          },
          (error: any) => {
            console.log(error.error);
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Error al Actualizar el Estado!',
              showConfirmButton: false,
              timer: 2500
            }
            );
            this.router.navigate(['/dashboard/listBossAnalist'])
            /* setTimeout(() => {
              window.location.reload();
            }, 2500) */
          }
        );
  }
}
