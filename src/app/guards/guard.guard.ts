import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class GuardGuard implements CanActivate {
  flag2: boolean =  false;
  
  constructor(private router:Router, private cookieservice: CookieService){
    
  }
  redirect(flag: boolean): any{
      if(!flag){
        this.router.navigate(['']);
      }
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
    const token = sessionStorage.getItem('Token');
    if(token == null){
        this.flag2 = false;
    }else{
        this.flag2 = true;
    }
    this.redirect(this.flag2);
    return this.flag2;
  }
  
}
