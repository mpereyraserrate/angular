import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  
  menuItems: any [];
  
  menuItems2: any [];
  analist_menu: any [];
  doctor_menu: any [];
  analist_boss_menu: any [];
  userName: string | null = sessionStorage.getItem('Usuario');
  /* user_id: string | null = this.cookieservice.get('user_id');
  token: string | null = this.cookieservice.get('Token');
  userMenu: string | null = this.cookieservice.get('Menu'); */
  /* userResponse: any [] = []; */
  
  public isloged: boolean;
  public isAdmin: boolean;
  public isJanalist: boolean;
  public isAnalist: boolean;
  public isSupplier: boolean;
  public isDoctor: boolean;
  constructor(private sidebarService: SidebarService, private router: Router,
              private cookieservice: CookieService,
              private login_service: LoginService) { 
      /* this.userResponse = JSON.parse(sessionStorage.getItem('Response') || '{}'); */
      this.isloged = false;
      this.isAdmin = false;
      this.isAnalist = false;
      this.isSupplier = false;
      this.isDoctor = false;
      this.isJanalist = false;
      /* console.log(this.userName);
      console.log(this.userMenu);
      console.log(this.user_id);
      console.log(this.token); */
      /* console.log(this.userResponse); */
      
      
      this.menuItems = sidebarService.menu;
      console.log(this.menuItems);

      this.menuItems2 = sidebarService.menu2;
      this.analist_menu = sidebarService.analist_menu;
      this.doctor_menu = sidebarService.doctor_menu;
      this.analist_boss_menu = sidebarService.analist_boss_menu;

      console.log(this.menuItems2);
      this.check();

  }
  check(){
    let user_id = sessionStorage.getItem('user_id');
    let role: any = sessionStorage.getItem('role')!;
    /* role = JSON.parse(role); */

    console.log(typeof role);
    
   /*  for(let i = 1; i<= role.length; i++){
      if(role[i] != null){
        if(user_id == '6'){
            this.isAdmin = true;
        }
        if(role[i] == '[\"JefeAnalista\"]'){
            this.isJanalist = true;
        }
        if(role[i] == '[\"AnalistaCategoria\"]'){
          this.isAnalist = true;
        }
        if(role[i] == '[\"proveedor\"]'){
          this.isSupplier = true;
        }
        if(role[i] == '[\"ResponsableAprobador\"]'){
          this.isDoctor = true;
        }
      }
      console.log(role[i]);
    } */
    if(role != null){
      
      if(role == '[\"JefeAnalista\"]'){
          this.isJanalist = true;
      }
      if(role == '[\"AnalistaCategoria\"]'){
        this.isAnalist = true;
      }
      if(role == '[\"proveedor\"]'){
        this.isSupplier = true;
      }
      if(role == '[\"ResponsableAprobador\"]'){
        this.isDoctor = true;
      }
    }
  }
  ngOnInit(): void {
  }
  logOut(){
    /* sessionStorage.removeItem('Usuario'); */
    /* this.cookieservice.delete('Usuario');
    this.cookieservice.delete('Menu');
    this.cookieservice.delete('Token');
    this.cookieservice.delete('user_id');
    this.cookieservice.deleteAll(); */
    sessionStorage.removeItem('Usuario');
    sessionStorage.removeItem('Menu');
    sessionStorage.removeItem('Token');
    sessionStorage.removeItem('user_id');
    sessionStorage.clear();
    /* sessionStorage.removeItem('Response'); */
    this.router.navigate(['/login']);
  }
}
