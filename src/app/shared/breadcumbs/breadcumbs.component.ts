import { Component, OnInit } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { filter, map } from "rxjs/operators";
@Component({
  selector: 'app-breadcumbs',
  templateUrl: './breadcumbs.component.html',
  styleUrls: ['./breadcumbs.component.css']
})
export class BreadcumbsComponent implements OnInit {
  
  public title: string = "";

  constructor(private router: Router) { 

    this.getArguments();
  }
  
  getArguments(){
    this.router.events
        .pipe(
          filter((event: any) => event instanceof ActivationEnd),
          filter((event: ActivationEnd) => event.snapshot.firstChild === null),  
          map((event: ActivationEnd) => event.snapshot.data)
        )
        .subscribe(({title}) => {
          
          this.title = title;

        })
  }
  ngOnInit(): void {
  }

}
