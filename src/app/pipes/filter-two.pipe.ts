import { Pipe, PipeTransform } from '@angular/core';
import { filter } from 'rxjs/operators';

@Pipe({
  name: 'filterTwo'
})
export class FilterTwoPipe implements PipeTransform {

  transform(value: any,args: string,args2: string): any {
    if(!value){
      return null;
    }
    if(args === ""){
      return value;
    }
    if(args2 === ""){
      return value;
    }
    const array: any [] = [];
    
    for(let i=0; i<= value.length; i++){
      let product_name: string = value[i]?.product.toLowerCase();
      let product_description: string = value[i]?.description;
      if(product_name?.startsWith(args.toLowerCase()) && product_description?.startsWith(args2)){
        array.push(value[i]);
      }
    }
    return array;
  }

}
