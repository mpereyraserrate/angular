import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterFive'
})
export class FilterFivePipe implements PipeTransform {

  transform(value: any, args: string): any {
    
    const resultPosts = [];
    
    const post = value;
    if(!value){
      return null;
    }
    if(args === ""){
      return value;
    }
    for(const post of value){
      if(post.sublinea.toLowerCase().indexOf(args.toLowerCase()) > -1){
        
        //console.log('Si');
        resultPosts.push(post);
        
      }
    }
    return resultPosts;

    
  }

}
