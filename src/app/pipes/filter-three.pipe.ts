import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterThree'
})
export class FilterThreePipe implements PipeTransform {

  transform(value: any, args: string): any {
    /* const resultPosts = []; */
    const resultPosts = [];
    
    const post = value;
    if(!value){
      return null;
    }
    if(args === ""){
      return value;
    }
    for(const post of value){
      /* if(post.estado.toString().indexOf(args) > -1){
          //console.log('Si');
          resultPosts.push(post);
          
      } */
      if(post.estado.indexOf(args) > -1){
        //console.log('Si');
        resultPosts.push(post);
        
    }
    }
    return resultPosts;

    
  }
}
