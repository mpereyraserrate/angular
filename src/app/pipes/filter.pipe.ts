import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, args: string): any {
    const resultPosts = [];
    
    const post = value;
    
    for(const post of value){
      if(post.description.toLowerCase().indexOf(args.toLowerCase()) > -1){
        
          //console.log('Si');
          resultPosts.push(post);
          
      }
    }
    return resultPosts;
  }

}
