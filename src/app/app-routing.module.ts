import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { UsersComponent } from './pages/users/users.component';
import { ProductsComponent } from './pages/products/products.component';
import { HeaderComponent } from "./shared/header/header.component";
import { PagesRoutingModule } from './pages/pages-routing.module';
import { NopageFoundComponent } from './nopage-found/nopage-found/nopage-found.component';
import { AuthRoutingModule } from './auth/auth-routing.module';


const routes: Routes = [
    {path: '', redirectTo:'/login' , pathMatch:'full'},
    {path: 'nopagefound', component: NopageFoundComponent},
    { path: '**', redirectTo:'nopagefound', pathMatch:'full'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
        PagesRoutingModule,
        AuthRoutingModule
    ],
    exports: [RouterModule]
})

export class AppRoutingModule{}