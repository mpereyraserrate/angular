import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { invalid } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  public username: string | undefined;
  public password: string | undefined;
  public codigoSistema: string | undefined;
  public userthree: [] = [];
  public user_id: Array <any>;
  form!: FormGroup;
  
  constructor(private router: Router, private fb: FormBuilder, private loginService: LoginService) { 
    this.user_id = [];
    this.makeForm();
  }
  
  ngOnInit(): void {
  }
  makeForm(){
    this.form = this.fb.group({
      email: ['',[ Validators.required,/*  Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$') */]],
      password: ['', [Validators.required, Validators.minLength(8)]],
    });
  }
  get emailField(): AbstractControl | null | undefined{
    return this.form?.get('email');
  }
  get emailValid(){
    return this.emailField?.touched && this.emailField?.valid;
  }
  get emailInvalid(){
    return this.emailField?.invalid && this.emailField.touched;
  }
  get passwordField(): AbstractControl | null | undefined{
    return this.form?.get('password');
  }
  get passwordValid(){
    return this.passwordField?.touched && this.passwordField?.valid;
  }
  get passwordInvalid(){
    return this.passwordField?.invalid && this.passwordField?.touched;
  }
  
  save(){
    console.log(this.form);

    if( this.form.invalid){
          return Object.values(this.form.controls).forEach( control =>{
          
            control.markAllAsTouched();
        });
      
    }
    this.router.navigate(['/dashboard'])
        .then(()=>{
          window.location.reload();
        })
    
  }
  redirect(){
    this.router.navigate(['/dashboard'])
        .then(()=>{
          window.location.reload();
        })
  }
  saveData(){
    let user = {
        username: this.username,
        password: this.password,
        codigoSistema: "PRODUCTOS"
    };
    this.loginService.postLogin(user)
        .subscribe(
          (res: any) => {
            console.log(res);
          },
          (error: any) => {
            /* alert(error.error); */
            console.log(error);
            
          }
         
        )
  }
  
}
