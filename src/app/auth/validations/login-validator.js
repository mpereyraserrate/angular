
$(function(){
    var $registerForm = $("registration");

    if($registerForm.length){
        $registerForm.validate({
            rules: {
                email: {
                    required: true
                }
            },
            messages: {
                email: {
                    required: 'El correo es obligatorio para iniciar sesión'
                }
            }
        })
    }
})
