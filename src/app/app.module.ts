import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";


//Modules
import { SharedModule } from "./shared/shared.module";
import { PagesModule } from './pages/pages.module';

//Import Routes
//Imports Material
import {MatSidenavModule} from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NopageFoundComponent } from './nopage-found/nopage-found/nopage-found.component';
import { AuthModule } from './auth/auth.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterSevenPipe } from './pipes/filter-seven.pipe';








@NgModule({
  declarations: [
    AppComponent,
    NopageFoundComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    PagesModule,
    AuthModule,
    FormsModule,
    HttpClientModule,
    // * MATERIAL IMPORTS
    MatSidenavModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    NgxPaginationModule
  ],
  exports: [
    NgxPaginationModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
