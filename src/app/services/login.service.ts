import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import Swal from 'sweetalert2'
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
private url: string = "http://192.168.0.75:1007/api/";
/* private url: string = "http://192.168.0.75:4200/api/"; */
  private headers: HttpHeaders | undefined;

  users: string  = "";
  constructor(private http: HttpClient, private router: Router, 
              private cookieservice: CookieService) { 

  }
  /* getQuery( query: string){
    const url= `http://192.168.0.75:1007/api/${query}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDOMGISqIaE5ogqmtw2Yh7wZ7tH63pnuDPpZav9n6kLKkrCkl7MXTv--KEU2VCPJkFGLDIgWmaQ4Anp_Qw'
   });

   return this.http.get( url, {headers});
   } */
   postLogin(user: any){
       
    return this.http.post( this.url + "Login", user/* , {headers:this.headers} */)
                    .pipe(map((resp: any) => {
                        
                        this.users = resp;
                        
                        console.log("La respuesta: ", this.users);
                        
                        if( resp.estado == 1){
                          sessionStorage.setItem("Usuario",resp.detalle.nombre);
                          /* this.cookieservice.set('Usuario',resp.detalle.nombre, 1, '/'); */
                          sessionStorage.setItem("Menu",resp.detalle.menuarbol);
                         /*  this.cookieservice.set('Menu',resp.detalle.menuarbol, 1, '/'); */
                          sessionStorage.setItem("Token",resp.detalle.authtoken);
                          /* this.cookieservice.set('Token',resp.detalle.authtoken, 1, '/'); */
                          sessionStorage.setItem("user_id",resp.detalle.userid);
                          /* this.cookieservice.set('user_id',resp.detalle.userid, 1, '/'); */
                          sessionStorage.setItem("role",resp.detalle.perfil);
                          /* this.cookieservice.set('role',resp.detalle.perfil, 1, '/'); */
                          /* sessionStorage.setItem("Response",JSON.stringify(resp)); */
                          Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: resp.mensaje,
                            showConfirmButton: false,
                            timer: 1500
                          }
                          );
                          this.router.navigate(['/dashboard'])
                                      setTimeout(() => {
                                        window.location.reload();
                                      }, 1500); // Activate after 5 minutes.
                                      /* .then(()=>{
                                        window.location.reload();
                                      }); */
                        }
                        if( resp.estado == 2){
                          Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: resp.mensaje,
                            showConfirmButton: false,
                            timer: 2000
                          }
                          );
                        }
                        if( resp.estado == 3){
                          Swal.fire({
                            position: 'top-end',
                            icon: 'error',
                            title: resp.mensaje,
                            showConfirmButton: false,
                            timer: 2000
                          }
                          );
                        }
                        return true;
                      }),
                    catchError((err) => {
                      console.log(err);
                      
                      Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'ERROR DEL SERVIDOR!!',
                        showConfirmButton: false,
                        timer: 3000
                      }
                      );
                      return throwError(err);
                    })
          );

   }
   setHeaders() {
    this.headers = new HttpHeaders()
      .set("Content-Type", "application/json")
      .set("X-Requested-With", "XMLHttpRequest");
  }
  
}
