import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  


  menu: any [] = [
    {
      title: 'Dashboard',
      icon: 'nav-icon fas fa-tachometer-alt',
      submenu: [
        {title: 'Lista Jefe Analista', url: 'listBossAnalist'},
        {title: 'Lista Productos Analista', url: 'listAnalists'},
        {title: 'Lista Productos Doctor', url: 'productListDoctors'},
        {title: 'Form Analistas', url: 'formAnalists'},
      ]
    }
  ]
  menu2: any = [
    {
      title: 'Gestión Productos',
      icon: 'nav-icon fas fa-tachometer-alt',
      submenu: [
        {
          title: 'Registrar Productos',
          icon: 'nav-icon fas fa-tachometer-alt',
          submenu2: [
            {
              title: 'Registro Productos',
              url: 'productos',
              router: 'addproducts'
            },
            /* {
              title: 'Actualizar Productos',
              url: 'productos',
              router: ''
            }, */
            {
              title: 'Lista de Productos',
              url: 'productos',
              router: 'productList'
            }
          ]
        },
      ],
    }
  ]
  doctor_menu: any [] = [
    {
      title: 'Dashboard',
      icon: 'nav-icon fas fa-tachometer-alt',
      submenu: [
        {title: 'Lista Productos Doctor', url: 'productListDoctors'},
      ]
    }
  ]
  analist_menu: any [] = [
    {
      title: 'Dashboard',
      icon: 'nav-icon fas fa-tachometer-alt',
      submenu: [
        {title: 'Lista Productos Analista', url: 'listAnalists'},
      ]
    }
  ]
  analist_boss_menu: any [] = [
    {
      title: 'Dashboard',
      icon: 'nav-icon fas fa-tachometer-alt',
      submenu: [
        {title: 'Lista Jefe Analista', url: 'listBossAnalist'},
        {title: 'Asignación Proveedores', url: 'supplierAssignments'},
        {title: 'Asignación Analistas', url: 'analistAssignments'},
      ]
    }
  ]
  constructor( ) { }
}
