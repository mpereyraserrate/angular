import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AssignmentService {
  private url: string = "http://192.168.0.75:1007/api/";
  private headers: HttpHeaders | undefined;
  constructor( private http: HttpClient, private router: Router) { 


  }

  get_perfil_users(users: any){
    return this.http.post( this.url + "Bridge/listarUsuarioPerfil", users);
  }
  get_lines(lines: any){
    return this.http.post( this.url + "Bridge/listarLinea", lines);
  }
  get_sub_lines(sub_lines: any){
    return this.http.post( this.url + "Bridge/listarSubLinea", sub_lines);
  }
  get_groups(groups: any){
    return this.http.post( this.url + "Bridge/listarGrupo", groups);
  }
  save_supplier_assignments(information: any){
    return this.http.post( this.url + "Asignacion/guardarAsignacionProveedor", information);
  }
  save_analist_assignments(information: any){
    return this.http.post( this.url + "Asignacion/guardarAsignacionAnalista", information);
  }
  get_supplier_assignments(information: any){
    return this.http.post( this.url + "Asignacion/listadoAsignacionProveedor", information);
  }
  get_suppliers_assignments(information: any){
    return this.http.post( this.url + "Asignacion/listadoAsignacionProveedor", information);
  }
  get_analists_assignments(information: any){
    return this.http.post( this.url + "Asignacion/listadoAsignacionAnalista", information);
  }
}
