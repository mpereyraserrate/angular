import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, delay, map } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private url: string = "http://192.168.0.75:1007/api/";
  private headers: HttpHeaders | undefined;
  products: any [] = [
    {
      id: '1',
      product: 'Paracetamol',
      description: 'en proceso'
    },
    {
      id: '2',
      product: 'Aspirina',
      description: 'rechazado'
    },
    {
      id: '3',
      product: 'alcohol',
      description: 'en proceso'
    },
    {
      id: '4',
      product: 'Soda',
      description: 'rechazado'
    },
    {
      id: '5',
      product: 'Galletas',
      description: 'en proceso'
    },
    {
      id: '6',
      product: 'Chicle',
      description: 'aceptado'
    },
    {
      id: '7',
      product: 'Diclofenaco',
      description: 'aceptado'
    },
  ]
  states: any [] = [
    {
      id: 1,
      description: 'en proceso'
    },
    {
      id: 2,
      description: 'aceptado'
    },
    {
      id: 3,
      description: 'rechazado'
    },
  ]
  constructor( private http: HttpClient, private router: Router) { 
    console.log('Spotify Service Ready');
  }
  
  /* getQuery( query: string){
    const url= `https://api.spotify.com/v1/${query}`;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDOMGISqIaE5ogqmtw2Yh7wZ7tH63pnuDPpZav9n6kLKkrCkl7MXTv--KEU2VCPJkFGLDIgWmaQ4Anp_Qw'
   });

   return this.http.get( url, {headers});
   } */
   /* getNewReleases(){
     
      return this.getQuery('browse/new-releases?limit=2')
             .pipe( map( (data: any) => {
               return data['albums'].items;
             }));
   } */
   get_list_container(user: any){
       
    return this.http.post( this.url + "Container/listarContainer",user/* , {headers:this.headers} */);
   }
   get_list_pharmaceutical(user: any){
     return this.http.post( this.url + "Pharmaceutical/listarPharmaceutical", user);
   }
   get_list_unit(user: any){
    return this.http.post( this.url + "Unit/listarUnit", user);
   }
   get_list_precondition(user: any){
    return this.http.post( this.url + "precondition/listarPrecondition", user);
   }
   get_list_consumption(user: any){
    return this.http.post( this.url + "Type/listarType", user);
   }
   all_states(state: any){
    return this.http.post( this.url + "Estados/listarEstados", state);
   }
   save_product(product_form: any){
     return this.http.post( this.url + "product/guardarProduct", product_form);
   }
   save_image(file: any){
    return this.http.post( this.url + "Product/guardarImage", file, {headers: this.headers});     
  }
   get_all_products(products: any){
     return this.http.post( this.url + "Product/listarProductos", products);
   }
   get_all_products2(products: any){
     return this.http.post( this.url + "Product/listarProductos", products);
   }
   get_all_products3(products: any){
     return this.http.post( this.url + "Product/listarProductos", products);
   }
   get_all_products4(products: any){
     return this.http.post( this.url + "Product/listarProductos", products);
   }
   get_all_products5(products: any){
     return this.http.post( this.url + "Product/listarProductos", products);
   }
   get_product_id(product_id: any){
      return this.http.post( this.url + "Product/listarProductosId", product_id);
   }
   update_states(product: any){
      return this.http.put( this.url + "Product/actualizarEstado", product);
   }
   save_information_analists(form: any){
     return this.http.post( this.url + "Product/guardarProductAnalist", form/* , {headers:this.headers} */);
   }
   checkProduct(email: any){
      return of({isProductAvailable: email !== 'jabon'})
              .pipe(
                delay(500)
              );
   }
   setHeaders() {
    this.headers = new HttpHeaders()
      .set("Content-Accept", "application/json")
      .set("X-Requested-With", "XMLHttpRequest")
      .set("Content-Type", "application/json");
  }
}
