
import { AbstractControl, AsyncValidatorFn } from "@angular/forms";
import { map } from "rxjs/operators";
import { ProductService } from '../services/product.service';


export class FormValidations {
    


    static validateProduct(productService: ProductService) {
        return(control: AbstractControl) => {
            const value = control.value;
            return productService.checkProduct(value)
                   .pipe(
                       map( response => {
                           return response.isProductAvailable ? null: {notAvailable: true};
                       })
                   )
        };        
    }
}